<?php

/**
 * Scraper to fetch data from DirectIndustry.com.
 * 
 * PHP version 7.*
 * 
 * @category PHP.
 * @package  None
 * @author   Mubin <mbnmughal30@gmail.com>
 * @license  MIT, https://opensource.org/licenses/MIT
 * @link     http://www.directindustry.com/
 */

/**
 * DirectIndustry Scraper.
 * 
 * PHP version 7.*
 * 
 * @category PHP.
 * @package  None
 * @author   Mubin <mbnmughal30@gmail.com>
 * @license  MIT, https://opensource.org/licenses/MIT
 * @link     http://www.directindustry.com/
 */
class DirectIndustry
{
    protected $baseUrl;
    protected $spider;
    protected $headers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->spider = new Spider;
        $this->headers = ['X-Requested-With' => 'XMLHttpRequest'];
        $this->baseUrl = 'http://www.directindustry.com/ajax/menu/products.html';
    }

    /**
     * App initializer.
     *
     * @return void
     */
    public function init()
    {
        $this->spider->spider(false, false, $this->baseUrl);
    }

    /**
     * Fetch categories and links to inner categories.
     *
     * @return array
     */
    public function fetchCategories()
    {
        $page = $this->spider->spider($this->headers, false, $this->baseUrl);
        if (!$page) {
            return [];
        }
        $page = str_get_html($page);
        $links = [];
        foreach ($page->find('.univers-group') as $universeGroup) {
            $li = $universeGroup->find('li', 0);
            $heading = trim($li->find('span', 0)->plaintext);
            foreach ($universeGroup->find('a') as $a) {
                $links[$heading][] = [
                    'url' => $a->href,
                    'subCategory' => trim($a->plaintext)
                ];
                $a->clear();
            }
            $universeGroup->clear();
        }
        $page->clear();
        return $links;
    }

    /**
     * Fetch sub catories with names for provided main category
     *
     * @param string $categoryPage url to sub category page
     * 
     * @return array
     */
    public function fetchSubCateogryLinks($categoryPage)
    {
        
        $links = [];
        $subPage = $this->spider->spider(false, false, $categoryPage);
        if ($subPage) {
            $subPage = str_get_html($subPage);
            $categoryGroup = $subPage->find('#category-group', 0);
            foreach ($categoryGroup->find('a') as $a) {
                $links[trim($a->plaintext)] = $a->href;
                $a->clear();
            }
            $categoryGroup->clear();
            $subPage->clear();
            return $links;
        } else {
            echo "$categoryPage returned no content.".PHP_EOL;
            return $links;
        }
    }
    /**
     * Fetches Products.
     *
     * @param string $productPageUrl 
     * 
     * @return array
     */
    public function fetchProducts($productPageUrl)
    {
        $products = [];
        $currentProduct = 0;
        do {
            $next = false;
            $productsPage = $this->spider->spider(false, false, $productPageUrl);
            if (!$productsPage) {
                echo "$productPageUrl yields no products.".PHP_EOL;
                continue;
            };
            $productsPage = str_get_html($productsPage);
            foreach ($productsPage->find('.product-tile') as $product) {
                $productTags = $product->find('a', 0)->title;
                if ($productTags == '{{product.defLight}}') {
                    continue;
                }
                $products[$currentProduct]['thumbnailImageUrl']
                    = $product->find('img', 0)->{'data-src'};
                $products[$currentProduct]['productTags'] = $productTags;
                $products[$currentProduct]['productUrl'] = $product->find('a', 0)->href;
                $products[$currentProduct]['Brand']
                    = trim($product->find('.brand', 0)->plaintext);
                $products[$currentProduct]['companyLogoUrl']
                    = $product->find('img', 1)->{'data-src'};
                $products[$currentProduct]['CompanyName']
                    = $product->find('img', 1)->alt;
                ++$currentProduct;
                if ($currentProduct == 200) {
                    return $products;
                }
            }
            if ($productsPage->find('link[rel="next"]', 0) && isset($productsPage->find('link[rel="next"]', 0)->href)) {
                $productPageUrl = $productsPage->find('link[rel="next"]', 0)->href;
                $next = true;
            }
        } while ($next);
        return $products;
    }

    public function fetchProductDetails($url)
    {
        $details = [];
        $productPage = $this->spider->spider(false, false, $url);
        if (!$productPage) {
            return false;
        }
        $productPage = str_get_html($productPage);
        $details['description'] = trim(
            strip_tags(
                $productPage->find('.product-description-js', 0)->plaintext
            )
        );
        $details['image'][] = $productPage->find('meta[property="og:image"]', 0)->content;
        foreach ($productPage->find('#stand-product-thumbs-container li') as $li) {
            $details['image'][] = $li->find('a', 0)->href;
        }
        $details['image'] = array_filter($details['image']);
        if ($productPage->find('.product-detail', 0)) {
            $characterstics = $productPage->find('.product-detail', 0);
            foreach ($characterstics->find('li') as $li) {
                $heading = trim(str_replace(':', '', $li->find('.feature-title', 0)->plaintext));
                $details['charaterstics'][$heading] = trim($li->find('.feature-value', 0)->plaintext);
            }
        }
        foreach ($productPage->find('a[data-clickthrough]') as $a) {
            $company = json_decode(html_entity_decode($a->{'data-clickthrough'}));
            if ($company) {
                $details['companyWebsite'] = parse_url($company->link)['host'];
            } else {
                $details['companyWebsite'] = '';
            }
        }
        return $details;
    }
}