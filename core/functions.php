<?php

//require_once("core.php");





/**

 * Description of class

 *                      includes all the common features that will be used for scrapping the data, cleaning the data and other

 * @author Mubin Khalid

 */

$functions = new functions();

class functions

{

    public $line;

    public function __construct() {



        $this->line = "___________________________________________________________________________________";

    }

    public function getHiddenFields($html) {

        $fields = array();

		if($html->find('input[type=hidden]')){

			foreach ($html->find('input[type=hidden]') as $input) {

				$fields[$input->name] = html_entity_decode($input->value, ENT_QUOTES, 'UTF-8');

			}

		}

        return $fields;

    }

    //clear any cookie before start scraping

    public function clear(){

        file_put_contents('cookie.txt', '# Mubin Khalid <mubin.khalid@healthcarousel.com>'.PHP_EOL);

    }

    // truncate table

    public function truncate($table = 'job_requests'){

        $connection = connection::open_connection();

        mysqli_query($connection, "TRUNCATE $table;");

        connection::close_connection($connection);

    }


	// truncate temp id table
	public function truncateTempIdTable($vms = '')
	{
		$conn = mysql_connect("localhost", "jobs_alldbs", "jobs@alldbs");
		if (!$conn) {
			echo "Unable to connect to DB: " . mysql_error();
			exit;
		}
		mysql_select_db("jobs_temp_tables");

		$sql = "TRUNCATE temp_email_$vms;";

		if (mysql_query($sql)) {
			echo "Table cleared<br /><br />";
		}
		else {
			echo "Error: " . $sql . "<br>" . mysql_error($conn) . "<br /><br />";
		}

		mysql_close($conn);
	}



    public function fetchLogin($website){

        $connection = connection::open_connection();

        $query = "SELECT * FROM `login` WHERE `website` = '$website';";

        $data = mysqli_query($connection, $query);

        $logins = array();

        while($row = mysqli_fetch_assoc($data)){

            $logins[] = $row;

        }

        connection::close_connection($connection);

        return $logins;

    }



    //log the error

    public function logError($status = "404 Page Not Found") {

        if (is_file(ERROR_FILE)) {

            file_put_contents(ERROR_FILE, $status . PHP_EOL . $this->line . PHP_EOL, FILE_APPEND);

        }

    }



    public function getViewState($page) {

        preg_match('/__VIEWSTATE\" value=\"(.*)\"/i', $page, $matches);

        return "__VIEWSTATE=" . urlencode(trim($matches[1]));

    }

    public function getViewStateGenerator($page) {

        preg_match('/__VIEWSTATEGENERATOR\" value=\"(.*)\"/i', $page, $matches);

        return "__VIEWSTATEGENERATOR=" . urlencode(trim($matches[1]));

    }

    public function rename($path) {

        $link = (parse_url($path));

        return str_replace('/', 'M', $link['path']);

    }

    public function save_image($url) {

        if (!is_dir(IMG_PATH)) {

            mkdir(IMG_PATH, 0777, true);

        }

        $path = IMG_PATH . $this->rename($url);

        $contents = file_get_contents($url);

        if (file_put_contents($path, $contents)) {

            return 1;

        }

        else {

            return 0;

        }

    }

    public function normalize($string) {

        return strtolower($string);

    }

    public function GetHTMLFromDom($domNodeList) {

        $domDocument = new DOMDocument();

        foreach ($domNodeList as $node) {



            $domDocument->appendChild($domDocument->importNode($node, true));

            $domDocument->appendChild($domDocument->createTextNode(" "));

        }

        return $domDocument->saveHTML();

    }

	//6-7-2016 (MP) I am adding &amp; to the list of strings that this function cleans.

    public function clean_text($string) {

        $string = strip_tags($string);

        $string = str_replace("â€", "-", $string);

        $string = str_replace("&amp;", "&", $string);

        $string = str_replace("&nbsp;", " ", $string);

        $string = str_replace("                           ", " ", $string);

        $string = str_replace("&quot;", "", $string);

        $string = str_replace("\n", "", $string);

        $string = str_replace("\r", "", $string);

        $string = str_replace("\t", "", $string);

        $string = str_replace("â‚¬", "€", $string);

        $string = str_replace("\xBB", "", $string);

        $string = str_replace("Â»", "", $string);

        $string = str_replace(chr(194), "", $string);
        $string = str_replace("Â", "", $string);
        $string = str_replace("�", "", $string);
        $string = str_replace("â", "", $string);

        $string = trim($string);

        return $string;

    }

    public function clean_number($string) {

        $string = str_replace("$", "", $string);

        $string = str_replace("£", "", $string);

        $string = str_replace("€", "", $string);

        $string = str_replace(",", "", $string);

        $string = trim($string);

        $string = strip_tags($string);

        return $string;

    }

    public function beHuman() {

        sleep(rand(3, 7));

    }

    public function populateDB($data, $table = 'job_requests', $useQuery = false) {



        //open connection from the connection file

        //it'll reccieve table as a String and data in the form of array(key value pair) and adjust those key values to a query

        // $where is an array that will take array as first one and impose that as an 'WHERE' clause and will set its values.

        if (!$useQuery) $data['required_credentials'] = implode(', ', $data['required_credentials']);

        $connection = connection::open_connection();

        $fields = array();

        $values = array();

        $val = '';

        foreach ($data as $key => $value) {

            $fields[] = "`$key`";

            $values[] = "'" . mysqli_real_escape_string($connection, $value) . "'";

        }

        if (!$useQuery) $data['rate'] = mysqli_real_escape_string($connection, $data['rate']);

        if (count($fields) == count($values)) {

            $insert = implode(", ", $fields);

            $val = implode(", ", $values);

        }

        $val = utf8_encode($val);



        //mysql_set_charset('utf8');

        mysqli_query($connection, "SET NAMES 'utf8'");

        $sql = "INSERT INTO $table($insert) VALUES($val) ";

        if ($useQuery) {

            unset($useQuery['url']);

            $sql.= $this->createUpdate($connection, $useQuery);

        }

        else {

            $sql.= " ON DUPLICATE KEY UPDATE `start_date` = '" . ($data['start_date']) . "', `end_date` = '" . ($data['end_date']) . "', `rate` = '" . ($data['rate']) . "';";

        }



        //print($sql);

        mysqli_query($connection, $sql) or die(print (mysqli_error($connection)) . "<br />" . print ($sql));

        connection::close_connection($connection);

    }

    private function createUpdate($connection, $data = array()) {

        if (count($data) > 0) {

            $sql = " ON DUPLICATE KEY UPDATE ";

            $keys = array();

            foreach ($data as $key => $value) {

                $value = mysqli_real_escape_string($connection, $value);

                $keys[] = "`$key` = '$value'";

            }

            $sql.= implode(", ", $keys);

            return $sql;

        }

    }

    public function parseFile($fileName) {

        $csv = new parseCSV();

        $csv->auto($fileName);

        if (count($csv->data)) {

            return $csv->data;

        } else

            return false;

    }





    public function getRates($note){
      if (is_null($note) !== false || empty($note) !== false){
        return 0;
      }
      $note = strtolower($note);
      switch($note){
        /*case(preg_match("/(scha){1}\s+(rates){1}\s*(apply){1}/", $note) ? true : false):
          return "Varies";
          break;*/
        case(preg_match('/\$[0-9]{2,3}(\.[0-9]{2})?/', $note, $matches) ? true : false):
          $temp = (double)str_replace("$", "", $matches[0]);
          if ($temp >= 40){
            return $temp;
            break;
          }
        case(preg_match("/[0-9]{2,3}(\.[0-9]{2})?\s*(br|bill rate)/", $note, $matches) ? true : false):
          $temp = preg_replace('/(\.[0-9]{2})/', '', $matches[0]);
          $temp = (double)preg_replace('/\D/', '', $temp);
          if ($temp >= 40){
            return $temp;
            break;
          }
        case(preg_match("/(br|bill rate)\s*[0-9]{2,3}(\.[0-9]{2})?/", $note, $matches) ? true : false):
          $temp = preg_replace('/(\.[0-9]{2})/', '', $matches[0]);
          $temp = (double)preg_replace('/\D/', '', $temp);
          if ($temp >= 40){
            return $temp;
            break;
          }
        default:
          return 0;
          break;
        }
    }




    public function checkHoursPerDay($value){
    	switch ($value) {
        // Catches normal formats such as 8:00 AM - 5:00 PM
        case (preg_match("/[0-9]{1,4}:[0-9]{2}\s*(am|pm|AM|PM|a\.m\.|p\.m\.|A\.M\.|P\.M\.)\s*(-|to|\/)\s*[0-9]{1,4}:[0-9]{2}\s*(am|pm|AM|PM|a\.m\.|p\.m\.|)/", $value, $matches) ? true : false):
          return $this->getHoursPerDay($matches[0]);
          break;
        // Catches formats like 8a-5a
        case (preg_match("/[0-9]{1,4}:*[0-9]*\s*(p|a|P|A)(?i)(m|M)*\s*(-|to|\/)\s*[0-9]{1,4}:*[0-9]*\s*(p|a|P|A)(?i)(m|M)*/", $value, $matches) ? true : false):
          $time = $this->convertToTime($matches[0]);
          return $this->getHoursPerDay($time);
          break;
        // Catches formats like 40 Hours per week.  Assumes the hours per day is
        // Hours per week / 5
        case (preg_match("/[0-9]{1,3}\s*(hours|hour|hrs)\s*(\/|per)\s*(week|wk)/" ,$value, $matches) ? true : false):
          $time = (int)preg_replace("/\D/", "", $matches[0]);
          $time = $time/5;
          return $time;
          break;
        // Catches formats like 8hrs, 10 hours etc.
        case (preg_match("/[0-9]{1,2}\s*-*(?i)(hours|hour|hrs|hr)/" ,$value, $matches) ? true : false):
          $time = preg_replace("/\d\//", "", $matches[0]);
					$time = (int)preg_replace("/\D/", "", $time);
          return $time;
          break;
        // Catches formats in military time like 0800-1900
				case (preg_match("/[0-9]{3,4}\s*(-|to|\/)\s*[0-9]{3,4}/" ,$value, $matches) ? true : false):
		     	$time = $this->convertToTime($matches[0]);
		     	return $this->getHoursPerDay($time);
		     	break;
        // Catches formats like 8-5
        case (preg_match("/[0-9]{1,2}\s*-[0-9]{1,2}/" ,$value, $matches) ? true : false):
          $temp = explode("-", $matches[0]);

          if ($temp[1] < $temp[0]){
            $temp[1] += 12;
          }

          return abs($temp[1] - $temp[0]);
          break;
        // Catches formats like 10/12.  Assumes these are hours per day and gmp_random_bits
        // the largest
        case (preg_match("/[0-9]{1,2}\s*\/[0-9]{1,2}/" ,$value, $matches) ? true : false):
          $temp = explode("/", $matches[0]);

          if ($temp[1] > $temp[0]){
            return (int)$temp[1];
          }
          else{
            return (int)$temp[0];
          }
          break;
        // One person decided that 8a730p was a good format, so this grabs that.
        case (preg_match_all("/[0-9]{1,4}(a|am|p|pm)/" ,$value, $matches) ? true : false):
          $string = $matches[0][0]." - ".$matches[0][1];
          $string = $this->convertToTime($string);
          return $this->getHoursPerDay($string);
          break;
        default:
          return 0;
          break;
      }
    }




    public function getHoursPerDay($time){
      $dailyHours = 0;

      $regex = "/.*\(/";

      $time = preg_replace($regex, '', $time);
      $time = str_replace(")", "", $time);

      if (preg_match("/to/", $time)){
        $timeArray = explode("to", $time);
      }
      elseif (preg_match("/\//", $time)){
        $timeArray = explode("/", $time);
      }
      else{
        $timeArray = explode("-", $time);
      }

      if (date("a", strtotime($timeArray[0])) == "pm" && date('g', strtotime($timeArray[0])) > date('g', strtotime($timeArray[1]))){
        $timeArray[0] = str_replace("pm", "am", $timeArray[0]);
        $timeArray[1] = str_replace("am", "pm", $timeArray[1]);

        $beginTime = date("G", strtotime($timeArray[1]));
        $beginMinutes = date('i', strtotime(trim($timeArray[1])))/60;
        $endTime = date("G", strtotime($timeArray[0]));
        $endMinutes = date('i', strtotime(trim($timeArray[0])))/60;
      }
      else{
        $beginTime = date("G", strtotime($timeArray[0]));
        $beginMinutes = date('i', strtotime(trim($timeArray[0])))/60;
        $endTime = date("G", strtotime($timeArray[1]));
        $endMinutes = date('i', strtotime(trim($timeArray[1])))/60;
      }

      if ($beginMinutes > 0.5){
        $beginTime++;
      }

      if ($endMinutes > 0.5){
        $endTime++;
      }

      return (int)abs($endTime - $beginTime);
    }





    public function convertToTime($time){
        $output = "";

        if (preg_match("/to/", $time)){
          $timeArray = explode("to", $time);
        }
        elseif (preg_match("/\//", $time)){
          $timeArray = explode("/", $time);
        }
        else{
          $timeArray = explode("-", $time);
        }

        $timeArray[0] = preg_replace("/(am|a|AM|A)/", "am", $timeArray[0], 1);
        $timeArray[0] = preg_replace("/(pm|p|PM|P)/", "pm", $timeArray[0], 1);
        $timeArray[1] = preg_replace("/(am|a|AM|A)/", "am", $timeArray[1], 1);
        $timeArray[1] = preg_replace("/(pm|p|PM|P)/", "pm", $timeArray[1], 1);

        for ($i = 0; $i < sizeof($timeArray); $i++){
          if (preg_match("/\b[0-9]{4}(pm|p|am|a|PM|P|AM|A)*\b/", $timeArray[$i]) ? true : false){
            $tempArray = str_split($timeArray[$i]);

            if(($key = array_search(" ", $tempArray)) !== false){
              array_splice($tempArray, $key, 1);
            }

            $timeArray[$i] = "$tempArray[0]$tempArray[1]:";
            unset($tempArray[0]);
            unset($tempArray[1]);
            foreach ($tempArray as $temp){
              $timeArray[$i] .= $temp;
            }
            $timeArray[$i] = preg_replace("/(am|a|AM|A)/", "am", $timeArray[$i], 1);
            $timeArray[$i] = preg_replace("/(pm|p|PM|P)/", "pm", $timeArray[$i], 1);
          }
          if (preg_match("/\b[0-9]{3}(pm|p|am|a)*\b/", $timeArray[$i]) ? true : false){
            if (preg_match("/(am|a|AM|A)/", $timeArray[$i]) ? true : false){
              $timeArray[$i] = "0$timeArray[$i]";
              $timeArray[$i] = preg_replace("/(am|a|AM|A)/", "", $timeArray[$i], 1);
            }
            if (preg_match("/(pm|p|PM|P)/", $timeArray[$i]) ? true : false){
              $timeArray[$i] = preg_replace("/(pm|p|PM|P)/", "", $timeArray[$i], 1);
              $timeArray[$i] = $timeArray[$i] + 1200;
              $timeArray[$i] = (string)$timeArray[$i];
            }
          }
        }

        $beginHours = date('g:i a', strtotime($timeArray[0]));
        $endHours = date('g:i a', strtotime($timeArray[1]));

        $output = "$beginHours - $endHours";

        return $output;
      }





      public function setRunningStatus($vms, $status){
        $conn = mysqli_connect("crawler.hcllcnetwork.net", "jobs_alldbs", "jobs@alldbs", "jobs_reports");
        if (!$conn) {
          echo "Unable to connect to DB: " . mysqli_error();
          exit;
        }
        if ($status == 1){
            $sql = "UPDATE jobs_reports.vms_status SET currently_running = $status WHERE vms_table_name='$vms' AND division='THS';";
        }
        else{
          $sql = "UPDATE jobs_reports.vms_status SET currently_running = $status, last_scraped=CURRENT_TIMESTAMP WHERE vms_table_name='$vms' AND division='THS';";
        }
        if (mysqli_query($conn, $sql)) {
          echo "Table cleared<br /><br />";
        }
        else {
          echo "Error: " . $sql . "<br>" . mysqli_error($conn) . "<br /><br />";
        }

        mysqli_close($conn);
      }
}
?>
