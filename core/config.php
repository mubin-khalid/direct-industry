<?php
/**
 * Application Configs.
 * 
 * PHP version 7.*
 * 
 * @category PHP.
 * @package  Core
 * @author   Mubin <mbnmughal30@gmail.com>
 * @license  MIT, https://opensource.org/licenses/MIT
 * @link     None
 */
/**
 * Created by IntelliJ IDEA.
 * User: mk
 * Date: 08/10/2018
 * Time: 4:58 AM
 */

return [
    'mysql' => [
        'username' => 'root',
        'password' => 'root',
        'database' => 'ucombine',
        'host' => '127.0.0.1',
    ]
];