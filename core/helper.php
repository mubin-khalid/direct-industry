<?php

/**
 * Helpers.
 * 
 * PHP version 7.*
 * 
 * @category PHP.
 * @package  Core
 * @author   Mubin <mbnmughal30@gmail.com>
 * @license  MIT, https://opensource.org/licenses/MIT
 * @link     None
 */

/**
 * Helper class to assist applicaton to filter and save data.
 * 
 * PHP version 7.*
 * 
 * @category PHP.
 * @package  Core
 * @author   Mubin <mbnmughal30@gmail.com>
 * @license  MIT, https://opensource.org/licenses/MIT
 * @link     None
 */

class Helper
{
    protected $db;
    /**
     * Helper constructor
     */
    public function __construct()
    {
        $this->db = new Connection;
    }
    /**
     * Renames and normalize input name
     *
     * @param string $url        Image file url.
     * @param string $renameWith Image name that need to be replaced with
     * 
     * @return string
     */
    public function rename($url, $renameWith)
    {
        $ext = pathinfo(basename($url))['extension'];
        return strtolower(str_replace(['/', ' ', ], '_', $renameWith . ".$ext"));
    }

    /**
     * Downloads images from url
     *
     * @param string $url   url to fetch image from.
     * @param string $title Title to rename file with
     * 
     * @return void
     */
    public function saveImage($url, $title)
    {
        $imgesFolder = realpath('./pics');
        if (!is_dir($imgesFolder)) {
            mkdir($imgesFolder, 0777, true);
        }
        $fileName = $this->rename($url, $title);
        $path = $imgesFolder . '/' . $fileName;
        file_put_contents($path, file_get_contents($url));
        return $fileName;

    }

    /**
     * Returns Sub2Catory id, subcateogry id and category id from database.
     *
     * @param string $sub2Category Category to search.
     * 
     * @return array,null
     */
    public function searchSub2Category($sub2Category)
    {
        $link = $this->db->openConnection();
        $sql = "SELECT id, cat_id, subcat_id
                FROM sub2category WHERE name='$sub2Category';";
        $resultSet = mysqli_query($link, $sql);
        $this->db->closeConnection($link);
        return mysqli_fetch_assoc($resultSet);
    }
    public function slug($text)
    {
        $slug = trim(strtolower(str_replace([' ', '_', '.', '/', ','], '-', $text)));
        return str_replace(['---', '--'], '-', $slug);
    }
    public function searchCategory($category)
    {
        $link = $this->db->openConnection();
        $sql = "SELECT id FROM category WHERE cat_name='$category';";
        $resultSet = mysqli_query($link, $sql);
        if (mysqli_num_rows($resultSet)) {
            return mysqli_fetch_assoc($resultSet)['id'];
        }
        return false;
    }

    public function searchSubCategory($subCategory)
    {
        $link = $this->db->openConnection();
        $sql = "SELECT id FROM subcategory WHERE subcat_name='$subCategory';";
        $resultSet = mysqli_query($link, $sql);
        if (mysqli_num_rows($resultSet)) {
            return mysqli_fetch_assoc($resultSet)['id'];
        }
        return false;
    }

    public function saveCategory($category)
    {
        $slug = $this->slug($category);
        $link = $this->db->openConnection();
        $sql = "
            INSERT INTO category(
                cat_name, cat_slug, description, image, created
            )
            VALUES('$category', '$slug', '', '', NOW());
        ";
        mysqli_query($link, $sql);
        return mysqli_insert_id($link);
    }

    public function saveSubCategory($subCategory, $catId)
    {
        $link = $this->db->openConnection();
        $slug = $this->slug($subCategory);
        $sql = "
            INSERT INTO subcategory(
                cat_id, subcat_name, subcat_slug, description, image, created
            )
            VALUES($catId, '$subCategory', '$slug', 
                '', '', NOW()
            );
        ";
        mysqli_query($link, $sql);
        return mysqli_insert_id($link);
    }

    public function saveSub2Category($sub2Category, $catId, $subCatId)
    {
        $slug = $this->slug($sub2Category);
        $link = $this->db->openConnection();
        $sql = "
            INSERT INTO sub2category(
                cat_id, subcat_id, name, slug, 
                description, image, created
            )
            VALUES($catId, $subCatId, '$sub2Category', '$slug', 
                '', '', NOW()
            );
        ";
        mysqli_query($link, $sql);
        return mysqli_insert_id($link);
    }

    public function searchThirdFilter($filter)
    {
        $link = $this->db->openConnection();
        $sql = "SELECT f_id, s_id, id FROM third_filter WHERE name='$filter';";
        $resultSet = mysqli_query($link, $sql);
        if (mysqli_num_rows($resultSet)) {
            return mysqli_fetch_assoc($resultSet);
        }
        return false;
    }
    public function searchSecondFilter($filter)
    {
        $link = $this->db->openConnection();
        $sql = "SELECT id FROM second_filter WHERE name='$filter';";
        $resultSet = mysqli_query($link, $sql);
        if (mysqli_num_rows($resultSet)) {
            return mysqli_fetch_assoc($resultSet)['id'];
        }
        return false;
    }

    public function searchFirstFilter($filter)
    {
        $link = $this->db->openConnection();
        $sql = "SELECT id FROM first_filter WHERE name='$filter';";
        $resultSet = mysqli_query($link, $sql);
        if (mysqli_num_rows($resultSet)) {
            return mysqli_fetch_assoc($resultSet)['id'];
        }
        return false;
    }
    public function saveThirdFilter($filter, $fId, $sId)
    {
        $link = $this->db->openConnection();
        $sql = "
            INSERT INTO third_filter(
                f_id, s_id, name
            )
            VALUES($fId, $sId, $filter);
        ";
        mysqli_query($link, $sql);
        return mysqli_insert_id($link);
    }
    public function saveSecondFilter($filter, $fId)
    {
        $link = $this->db->openConnection();
        $sql = "
            INSERT INTO second_filter(
                f_id, name
            )
            VALUES($fId, '$filter');
        ";
        mysqli_query($link, $sql);
        return mysqli_insert_id($link);
    }

    public function saveFirstFilter($filter)
    {
        $link = $this->db->openConnection();
        $sql = "
            INSERT INTO first_filter(
                name
            )
            VALUES('$filter');
        ";
        mysqli_query($link, $sql);
        return mysqli_insert_id($link);
    }


    public function saveProduct($product)
    {
        echo "$product[productTags]".PHP_EOL;
        $slug = $this->slug($product['productTags']);
        if ($this->searchProduct($slug)) {
            //return;
        }
        $link = $this->db->openConnection();
        $filters = $this->filters($product['details']['charaterstics']);
        $companyId = $this->searchCompany($product['CompanyName'], $product['details']['companyWebsite']);
        $userId = $companyId ? $companyId : $this->saveCompany(
            [
                'name' => $product['CompanyName'],
                'url' => $product['companyLogoUrl'],
                'website' => $product['details']['companyWebsite'],
            ]
        );
        $images = [];
        foreach ($product['details']['image'] as $index => $path) {
            $images[] = $this->saveImage($path, $slug . "-$index");
        }
        $images = implode('$-$', $images);
        $sql = "INSERT INTO product(
            user_id, requested, cat_id, subcat_id, sub2cat_id, first_filter_id, 
            second_filter_id, third_filter_id, product_name, slug, multi_image,
            description, short_descritption, created
        ) VALUES($userId, 'approved', {$product[ids]['cat_id']}, 
        {$product[ids][subcat_id]},
        {$product[ids][id]}, '$filters[first_level]', '$filters[second_level]',
        '\$\$', '$product[productTags]', '$slug', '$images', 
        '{$product[details][description]}', '', NOW()
        )";
        mysqli_query($link, $sql);
    }
    public function searchProduct($slug)
    {
        $link = $this->db->openConnection();
        $sql = "SELECT id FROM product WHERE slug='$slug';";
        $resultSet = mysqli_query($link, $sql);
        if (mysqli_num_rows($resultSet)) {
            return mysqli_fetch_assoc($resultSet)['id'];
        }
        return false;
    }
    public function saveCompany($company)
    {
        $faker = Faker\Factory::create();
        $logo = $this->saveImage($company['url'], $company['name']);
        $link = $this->db->openConnection();
        $sql = "
            INSERT INTO registeruser(
                user_type, country_id, city_id, type_of_company, type_of_sector, 
                full_name, email, password, telephone_number, website, description,
                logo, banner,reg_code, user_status, created
            )
            VALUES('individual_type', '$faker->countryCode', 
            $faker->randomDigitNotNull, 'Industrial Manufacturer', 'Electronics', 
            '$company[name]', '$faker->email', '$faker->userName', 
            '$faker->phoneNumber', '$company[website]', '$faker->text',
            '$logo', '$logo', 0, 1, NOW()
        );
        ";
        mysqli_query($link, $sql);
        return mysqli_insert_id($link);
    }

    public function searchCompany($company, $website)
    {
        $link = $this->db->openConnection();
        $sql = "SELECT id FROM registeruser WHERE full_name='$company';";
        $resultSet = mysqli_query($link, $sql);
        $id = mysqli_fetch_assoc($resultSet)['id'];
        if (mysqli_num_rows($resultSet)) {
            if ($website != '') {
                mysqli_query($link, "UPDATE registeruser SET website='$website' WHERE id = $id");
            }
            return $id;
        }
        return false;
    }
    public function filters($filters)
    {
        $filterIds = [];
        $fids = [];
        $sids = [];
        if (!is_array($filters)) {
            return [
                'first_level' => '$$',
                'second_level' => '$$',
            ];
        }
        $filters = array_filter($filters);
        foreach ($filters as $firstLevel => $secondLevel) {

            $fid = $this->searchFirstFilter($firstLevel);
            if (!$fid) {
                $fid = $this->saveFirstFilter($firstLevel);
            }

            $fids[] = $fid;
            // $sId = $this->searchSecondFilter(trim($secondLevel));
            // if (!$sId) {
            //     $sId = $this->saveSecondFilter($secondLevel, $fid);
            // }
            //$sids[] = $sId;
            $subFilters = explode(',', $secondLevel);
            foreach ($subFilters as $filter) {
                $sId = $this->searchSecondFilter(trim($filter));
                if (!$sId) {
                    $sId = $this->saveSecondFilter($filter, $fid);
                }
                $sids[] = $sId;
            }

            $filterIds[] = implode('$', $sids);
        }
        return [
            'first_level' => '$' . implode('$', $fids) . '$',
            'second_level' => str_replace(['$$', '$$$'], '$', '$' . implode('$', $filterIds) . '$'),
        ];
    }
    // public function extractFilters($name)
    // {
    //     $name = explode('/', $name);
    //     if (isset($name[1]) && $name[1] !== '') {
    //         unset($name[1]);
    //         $filterIds = [];
    //         foreach ($name as $tag) {
    //             $filters = $this->searchThirdFilter(trim($tag));
    //             if ($filters) {
    //                 $filterIds['t_id'][] = $filters['id'];
    //                 $filterIds['s_id'][] = $filters['s_id'];
    //                 $filterIds['f_id'][] = $filters['f_id'];
    //             } else {
    //                 $fid = $this->saveFirstFilter();
    //                 $sid = $this->
    //                 $filterIds['f_id'][] = $fid;
    //             }
    //         }
    //         $filterIds['t_id'] = '$' . implode('$', $filterIds['t_id']) . '$';
    //         $filterIds['s_id'] = '$' . implode('$', $filterIds['s_id']) . '$';
    //         $filterIds['f_id'] = '$' . implode('$', $filterIds['f_id']) . '$';
    //     }
    // }
}