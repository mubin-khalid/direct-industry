<?php

/**
 * PHP version 7.1
 * 
 * @category PHP
 * @package  Core
 * @author   Mubin <mbnmughal30@gmail.com>
 * @license  BSD Licence
 * @link     http://host/Scraper.php
 */


/**
 * PHP version 7.1
 * 
 * @category Application_Boursorama
 * @package  Core
 * @author   Mubin <mbnmughal30@gmail.com>
 * @license  BSD Licence
 * @link     http://host/Scraper.php
 */

class Connection
{
    protected $config;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->config = include './core/config.php';
        $this->config = $this->config['mysql'];
    }

    /**
     * Opens connection to database
     *
     * @return MySQLi instance
     */
    public function openConnection()
    {
        $connection = mysqli_connect(
            $this->config['host'],
            $this->config['username'],
            $this->config['password'],
            $this->config['database']
        )
            or die(print(mysqli_error($connection)));
        mysqli_set_charset($connection, "utf8");
        return $connection;
    }

    /**
     * Closes connection to database.
     *
     * @param mysqli $connection connection instance.
     * 
     * @return void
     */
    public static function closeConnection($connection)
    {
        mysqli_close($connection);
    }

}
