<?php

/**
 * Description: Static Class to Scrape Page contents.
 *   		          
 * @author Mubin.
 */
class Spider
{
        //spider function will require a lot of arguments and that will be set as the "Header", Referer and other stuff
        // url -- the page you want to crawl
    public function __construct()
    {

    }
    public static function yql($url)
    {
        $yql_base_url = "http://query.yahooapis.com/v1/public/yql";
        $url = "select * from html where url = '$url'";

        $yql_query_url = $yql_base_url . "?q=" . urlencode($url);
        $yql_query_url .= "&format=json";
            //echo '<br />https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20html%20where%20url%20%3D%20%22http%3A%2F%2Ftrustedpros.ca%2Fmb%2Fwinnipeg%22&format=json&diagnostics=true&callback=';
        $session = curl_init($yql_query_url);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($session);
        curl_close($session);
        return json_decode($json, true);
    }
    public function spider(
        $header = array(),
        $referer = false,
        $url = '',
        $cookie = false,
        $post = false,
        $saveImage = false,
        $view_info = false
    ) {
        //sleep(rand(2, 4));
        if (!$cookie) {
            $cookie = "cookie.txt";
        }
        $ip = $this->_rotate_ip();
        if ($saveImage) {
            $fp = fopen($saveImage, 'w+');
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_FILE, $fp);
        } else {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
        }
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate,sdch');
        if (isset($header) && !empty($header)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }
        
        //curl_setopt($ch, CURLOPT_PROXY, $ip);
        //echo "$ip<br />";
        curl_setopt($ch, CURLOPT_HEADER, false);
        //curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt(
            $ch,
            CURLOPT_USERAGENT,
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/50.0.2661.102 Chrome/50.0.2661.102 Safari/537.36"
        );
        curl_setopt($ch, CURLOPT_COOKIEJAR, realpath($cookie));
        curl_setopt($ch, CURLOPT_COOKIEFILE, realpath($cookie));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        if (isset($referer) && $referer != false) {
            curl_setopt($ch, CURLOPT_REFERER, $referer);
        } else {
            curl_setopt($ch, CURLOPT_REFERER, $url);
        }
            //if have to post data on the server
        if (isset($post) && !empty($post) && $post) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        } //endif
        $data = curl_exec($ch);
        $info = curl_getinfo($ch);
        
        if ($view_info) {
            print_r($info);
        }
        curl_close($ch);
        if ($saveImage) {
            fclose($fp);
        }
        if ($info['http_code'] == 200) {
            return ($data);
        } else {

            return false;
        }

    }

    private function _rotate_ip()
    {
        $proxies = [
            '213.85.18.172:3128',
            '66.70.188.148:3128',
            '187.160.245.156:3128',
            '188.165.64.114:3128',
            '173.249.35.163:10010',
            '194.38.137.252:3128',
            '37.187.155.41:3128',
            '180.250.223.7:3128',
            '195.122.185.95:3128',
            '54.39.20.167:3128',
            '167.114.185.238:3128',
            '178.62.10.72:3128',
            '178.62.46.68:3128',
            '178.128.111.180:3128',
            '178.62.50.112:3128',
            '138.197.165.126:41766',
            '78.47.151.98:8888',
            '159.203.23.98:3128',
            '138.68.163.3:3128',
            '178.62.20.31:3128',
            '59.127.168.43:3128',
            '46.101.31.136:3128',
            '156.54.63.33:8080',
            '138.68.138.121:3128',
            '54.39.53.104:3128',
            '178.62.32.8:3128',
            '46.101.16.179:3128',
            '46.101.30.38:3128',
            '46.101.38.215:3128',
            '139.59.180.162:3128',
            '138.68.176.142:3128',
            '128.199.86.76:3128',
            '159.65.0.190:8888',
            '51.75.168.183:3128',
            '66.70.167.113:3128',
            '209.58.164.43:3128',
            '128.199.244.175:3128',
            '222.147.15.161:3128',
            '178.62.109.248:3128',
            '77.81.107.112:54321',
            '128.199.212.147:3128',
            '204.12.219.162:3128',
            '206.189.36.29:3128',
            '82.206.132.106:80',
            '186.18.153.228:3128',
            '178.62.43.128:3128',
            '138.68.170.117:3128',
            '159.89.197.59:3128',
            '46.101.51.179:3128',
            '207.148.101.248:3128',
            '5.39.91.73:3128',
            '128.199.158.234:3128',
            '46.101.36.172:3128',
            '46.101.56.19:3128'
        ];
        return $proxies[rand(0, count($proxies) - 1)];
    }
}
