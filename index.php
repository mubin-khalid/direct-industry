<?php

/**
 * Service entry point.
 * 
 * PHP version 7.*
 * 
 * @category PHP.
 * @package  None
 * @author   Mubin <mbnmughal30@gmail.com>
 * @license  MIT, https://opensource.org/licenses/MIT
 * @link     http://www.directindustry.com/
 */
require_once './vendor/autoload.php';
require_once './core/spider.php';
require_once './core/dom.php';
require_once './core/config.php';
require_once './core/connection.php';
require_once './core/helper.php';
require_once './DirectIndustry.php';
set_time_limit(0);
ini_set('memory_limit', -1);
error_reporting(E_CORE_ERROR);
$helper = new Helper;
$directIndustry = new DirectIndustry;
$links = $directIndustry->fetchCategories();
//$links = (array_reverse($links));
//$links = ['Hydraulics - Pneumatics' => $links['Hydraulics - Pneumatics']];
$urls = file('./urls.txt', FILE_IGNORE_NEW_LINES);
foreach ($links as $category => $subCategories) {
    //echo "Category: $category". PHP_EOL;
    echo "\033[31;1;4m$category\033[0m".PHP_EOL;
    $catIdFound = $helper->searchCategory($category);
    $catId = $catIdFound ? $catIdFound : $helper->saveCategory($category);
    foreach ($subCategories as $subCategory) {
        echo "\033[35m$subCategory[subCategory]\033[0m".PHP_EOL;
        if (in_array($subCategory['url'], $urls)) {
            echo "$subCategory[subCategory] already scraped, skipping.". PHP_EOL;
            continue;
        }
        $subCatFound = $helper->searchSubCategory($subCategory['subCategory']);
        $subCatId = $subCatFound ?
            $subCatFound :
            $helper->saveSubCategory($subCategory['subCategory'], $catId);
        //get subCategory id, save otherwise.
        $sub2Categories = $directIndustry->fetchSubCateogryLinks(
            $subCategory['url']
        );
        foreach ($sub2Categories as $sub2Category => $sub2CategoryUrl) {
            file_put_contents(
                'logs.txt',
                "1st level Category: $category, 2nd level Category: $subCategory[subCategory], 3rd level Category: $sub2Category" . PHP_EOL,
                FILE_APPEND
            );
            echo "\033[32m $sub2Category\033[0m". PHP_EOL;
            if (in_array($sub2CategoryUrl, $urls)) {
                echo "$sub2Category already scraped, skipping.". PHP_EOL;
                continue;
            }
            $productCategoryDetails = $helper->searchSub2Category($sub2Category);
            if (!$productCategoryDetails) {
                $productCategoryDetails['cat_id'] = $catId;
                $productCategoryDetails['subcat_id'] = $subCatId;
                $productCategoryDetails['id'] = $helper->saveSub2Category(
                    $sub2Category,
                    $catId,
                    $subCatId
                );
            }
            //print_r($productCategoryDetails);
            $products = $directIndustry->fetchProducts($sub2CategoryUrl);
            $products = array_slice($products, 0, 200);
            if (count($products) < 1) continue;
            foreach ($products as $key => $product) {
                if (in_array($product['productUrl'], $urls)) {
                    //echo "$product[productTags] already in bag, skipping.". PHP_EOL;
                    continue;
                }
                $details = $directIndustry->fetchProductDetails(
                    $product['productUrl']
                );
                if (!$details) {
                    continue;
                }
                $product['details'] = $details;
                $product['ids'] = $productCategoryDetails;
                $helper->saveProduct($product);
                file_put_contents('urls.txt', $product['productUrl'] . PHP_EOL, FILE_APPEND);
            }
            file_put_contents('urls.txt', $sub2CategoryUrl . PHP_EOL, FILE_APPEND);
        }
        file_put_contents('urls.txt', $subCategory['url'] . PHP_EOL, FILE_APPEND);
    }
    echo "\033[31;1;4m$category\033[0m".PHP_EOL;
}